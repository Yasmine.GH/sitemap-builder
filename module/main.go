package main

import (
	"flag"
	"log"
	"module/gethtml"
	"module/htmlparser"
	"module/xmlWriter"
)

func main() {

	flag.Parse()

	if flag.Arg(0) == "" {
		log.Fatal("Enter ths website url")

	}
	websiteUrl := flag.Arg(0)

	linksToVisit := []string{websiteUrl}
	visitedLinks := make(map[string]string)
	for idx := 0; idx < len(linksToVisit); idx++ {
		html, err := gethtml.GetHtmlPage(linksToVisit[idx])
		if err != nil {
			log.Fatal(err)
		}
		newLinks := htmlparser.Parse(string(html), websiteUrl)
		toVisit := checkIfVisited(visitedLinks, newLinks)
		linksToVisit = intersection(linksToVisit, toVisit)
		visitedLinks[linksToVisit[idx]] = linksToVisit[idx]

	}

	xmlWriter.CreateXml(visitedLinks, websiteUrl)

}

func checkIfVisited(visitedLinks map[string]string, linksToCheck []string) (linksToVisit []string) {
	linksToCheck = removeDups(linksToCheck)
	for _, link := range linksToCheck {
		if _, ok := visitedLinks[link]; !ok {
			linksToVisit = append(linksToVisit, link)
		}
	}
	return
}

func intersection(finalLinksToVisit []string, newLinksToVisit []string) []string {
	hash := make(map[string]bool)
	for _, e := range finalLinksToVisit {
		hash[e] = true
	}
	for _, e := range newLinksToVisit {
		// If elements present in the hashmap then append intersection list.
		if !hash[e] {
			finalLinksToVisit = append(finalLinksToVisit, e)
		}
	}
	return finalLinksToVisit
}

//Remove dups from slice.
func removeDups(elements []string) (noDups []string) {
	encountered := make(map[string]bool)
	for _, element := range elements {
		if !encountered[element] {
			noDups = append(noDups, element)
			encountered[element] = true
		}
	}
	return
}
