package xmlWriter

import (
	"encoding/xml"
	"fmt"
	"io"
	"log"
	"net/url"
	"os"
	"strings"
)

type Url struct {
	XMLName xml.Name `xml:"url"`
	Loc     string   `xml:"loc"`
}

type Urlset struct {
	XMLName xml.Name `xml:"urlset"`
	Xmlns   string   `xml:"xmlns,attr"`
	Urls    []Url    `xml:"url"`
}

func CreateXml(links map[string]string, webSiteUrl string) {

	v := &Urlset{Xmlns: webSiteUrl}

	// add two staff details
	for _, link := range links {
		fmt.Print(link)
		v.Urls = append(v.Urls, Url{Loc: link})
	}

	xmlFilename := getWebSiteName(webSiteUrl) + "-sitemap.xml"
	xmlFile, _ := os.Create(xmlFilename)
	xmlFile.WriteString(xml.Header)
	xmlWriter := io.Writer(xmlFile)

	enc := xml.NewEncoder(xmlWriter)

	enc.Indent("  ", "    ")
	if err := enc.Encode(v); err != nil {
		fmt.Printf("error: %v\n", err)
	}
}

func getWebSiteName(webSiteUrl string) (domain string) {
	u, err := url.Parse(webSiteUrl)
	if err != nil {
		log.Fatal(err)
	}
	parts := strings.Split(u.Hostname(), ".")
	domain = parts[len(parts)-2] + "." + parts[len(parts)-1]
	return
}
