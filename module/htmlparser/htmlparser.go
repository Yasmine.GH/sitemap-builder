package htmlparser

import (
	"strings"

	"golang.org/x/net/html"
)

func Parse(text string, websiteUrl string) (data []string) {

	tkn := html.NewTokenizer(strings.NewReader(text))

	var links []string

	for {

		tt := tkn.Next()

		switch {

		case tt == html.ErrorToken:
			return links

		case tt == html.StartTagToken:
			t := tkn.Token()
			if t.Data == "a" {

				for _, element := range t.Attr {

					if element.Key == "href" {
						if strings.Contains(element.Val, websiteUrl) {
							links = append(links, element.Val)
						} else if strings.HasPrefix(element.Val, "/") {
							links = append(links, websiteUrl+element.Val[1:])
						}
					}
				}
			}

		}
	}
}
